��    ,      |  ;   �      �  W  �     !  =   '  R   e     �     �     �     �     �  	   	  5        I     Q     a     h     t     |  
   �     �     �     �     �  .   �     �     �                1     >     G     g     z     �     �  U   �  �   �  
   �     �     �  T   �     	     	     	  A  "	  �  d
  	   �  I   �  v   H     �     �     �     �            Y   #     }     �  	   �     �  
   �     �     �     �     �  	   �     �  1   
     <  -   U  (   �     �     �     �  0   �           ?     P     g  `   o  �   �  
   �     �     �  o   �     3     ;  	   B           *      '                    "               +   &      )                (                          #      
                        !          %         ,             	                   $            
                <p>
                  Night before %(yesterday)s and %(current_day)s.<br />
                </p>
                <p class="helptext">
                  This is the data for the last night. It is shown here because it's missing some data. Once complete, it will disappear from your homepage.
                </p>
               Apply Audodo even lets you save your naps and nocturnal awakenings. Audodo is a free and open source sleep diary, where you can save your sleep data : Audodo logo Check the code Comments Condition during the day Condition on waking Dashboard Email with password reset instructions has been sent. English Forgot password French Get up hour History Home Last night Lay down hour Log in Login Logout Night before %(current_day)s and %(tomorrow)s. Password changed Password reset failed Password reset successfully Quality of sleep Register now Reset it Reset password at %(site_name)s Show/hide the form Sleep end hour Sleep start hour Submit The goal behind this tool is to let you discover the optimal sleep duration you need. This instance of audodo does not accept new members, go check another audodo instance, or <a href='https://gitlab.com/sodimel/audodo'>deploy your own</a>. This night Update Welcome to audodo! You can see statistics about your sleep schedule, and even export data to pdf files! account/ audodo login Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 
                <p>
                  Nuit entre le %(yesterday)s et le %(current_day)s.<br />
                </p>
                <p class="helptext">
                  Ces données concernent la nuit passée. Le formulaire est affiché ici parce qu'il n'est pas complete. Une fois renseigné dans son intégralité, il n'apparaîtra plussur la page d'accueil.
                </p>
               Appliquer Audodo vous permet également de noter vos siestes et réveils nocturnes. Audodo est un outil libre et open source de calendrier nocturne, où vous pouvez enregistrer vos données de sommeil : Logo de audodo Consultez le code Commentaires Forme de la journée État au réveil Tableau de bord Un email contenant les instructions de réinitialisation du mot de passe a été envoyé. Anglais Mot de passe oublié Français Heure du lever Historique Accueil Nuit dernière Heure du coucher Se connecter Connexion Déconnexion Nuit entre le %(current_day)s et le %(tomorrow)s. Mot de passe mis à jour Echec de la réinitialisation du mot de passe Mot de passe réinitialisé avec succès Qualité du sommeil Inscrivez-vous dès à présent Réinitialiser Réinitialiser le mot de passe sur %(site_name)s Afficher/masquer le formulaire Heure de réveil Heure d'endormissement Envoyer Le but derrière cet outil est de vous permettre de découvrir votre durée de sommeil optimale. Cette instance de audodo n'accepte pas de nouveaux membres, vous pouvez cependant vous rendre sur une autre instance, ou bien <a href='https://gitlab.com/sodimel/audodo'>déployer la votre</a>. Cette nuit Mettre à jour Bienvenue sur audodo ! Vous pouvez voir des statistiques relatives à votre rythme de sommeil, et même exporter des données en pfd ! compte/ audodo connexion 