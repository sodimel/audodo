from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.urls import include, path
from django.utils.translation import gettext_lazy as _

urlpatterns = [
    path("i18n/", include("django.conf.urls.i18n")),
]

urlpatterns += i18n_patterns(
    path("admin/", admin.site.urls),
    path(
        _("account/"), include(("django.contrib.auth.urls", "user"), namespace="user")
    ),
    path("", include("audodo.apps.sleep.urls", namespace="sleep")),
)
