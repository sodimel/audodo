from django.contrib import admin

from .models import Nap, NocturnalAwakening, SleepLogDay


@admin.register(SleepLogDay)
class SleepLogDayAdmin(admin.ModelAdmin):
    ...


@admin.register(NocturnalAwakening)
class NocturnalAwakeningAdmin(admin.ModelAdmin):
    ...


@admin.register(Nap)
class NapAdmin(admin.ModelAdmin):
    ...
