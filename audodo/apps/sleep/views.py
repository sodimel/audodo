from datetime import datetime, timedelta

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.views.generic import (
    CreateView,
    ListView,
    RedirectView,
    TemplateView,
    UpdateView,
    View,
)

from .forms import SleepLogDayForm
from .models import Nap, NocturnalAwakening, SleepLogDay
from .utils import get_all_days_for_month, get_next_month, get_previous_month


class Index(TemplateView):
    template_name = "base.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            current_day = timezone.now().date()
            yesterday = current_day - timedelta(1)
            last_night = SleepLogDay.objects.filter(
                user=self.request.user, day=yesterday
            ).first()
            if last_night and not last_night.is_complete:
                context["last_day"] = last_night
                context["yesterday"] = yesterday
                context["previous_sleep_log_day_form"] = SleepLogDayForm(
                    instance=last_night
                )
            context["sleep_log_day"] = SleepLogDay.objects.filter(
                user=self.request.user, day=current_day
            ).first()
            context["sleep_log_day_form"] = SleepLogDayForm(
                initial={"day": current_day, "user": self.request.user},
                instance=context["sleep_log_day"],
            )
            context["naps"] = Nap.objects.filter(
                user=self.request.user, day=current_day
            )
            context["nocturnal_awakenings"] = NocturnalAwakening.objects.filter(
                night=context["sleep_log_day"]
            )
            context["current_day"] = current_day
            context["tomorrow"] = current_day + timedelta(days=1)
        return context


class CreateOrUpdateCurrentDay(LoginRequiredMixin, View):
    def post(self, request):
        sleep_log_day, created = SleepLogDay.objects.get_or_create(
            user=self.request.user, day=timezone.now()
        )
        form = SleepLogDayForm(request.POST, instance=sleep_log_day)
        if form.is_valid():
            form.save()
            messages.success(request, _("Your current day data have been updated."))
        else:
            messages.error(
                request, _(f"There was an error or more on your form: {form.errors}")
            )
        return redirect(reverse("sleep:index"))


class History(LoginRequiredMixin, ListView):
    template_name = "sleep/history.html"
    model = SleepLogDay
    context_object_name = "nights"

    def get_queryset(self):
        qs = (
            super()
            .get_queryset()
            .filter(
                user=self.request.user,
                day__month=self.kwargs["month"],
                day__year=self.kwargs["year"],
            )
        )
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        current_month = timezone.now().month

        context["time"] = f"{self.kwargs['month']} {self.kwargs['year']}"

        previous_month, previous_year = get_previous_month(
            self.kwargs["month"], self.kwargs["year"]
        )
        context["previous_month"] = {
            "year": previous_year,
            "month": previous_month,
        }

        context["next_month"] = False
        if (self.kwargs["month"], self.kwargs["year"]) != (
            current_month,
            timezone.now().year,
        ):
            next_month, next_year = get_next_month(
                self.kwargs["month"], self.kwargs["year"]
            )
            context["next_month"] = {
                "year": next_year,
                "month": next_month,
            }

        context["all_days"] = get_all_days_for_month(
            self.kwargs["month"], self.kwargs["year"]
        )
        for i in range(len(context["all_days"])):
            context["all_days"][i] = datetime.strptime(
                f"{self.kwargs['year']}-{self.kwargs['month']}-{context['all_days'][i]}",
                "%Y-%m-%d",
            ).date()

        # TODO: fix this ugly function? idk, I want to include all existing days in this list I got, but I don't know how to do it outside the get_context_data function for now
        for night in self.get_queryset():
            context["all_days"][night.day.day - 1] = night

        context["all_days"].reverse()  # we want next day on top of the page

        context["days_to_come"] = []
        context["previous_days"] = context["all_days"]

        if current_month == self.kwargs["month"]:
            remaining_days_til_end_of_month = (
                len(context["all_days"]) - datetime.now().day
            )
            context["days_to_come"] = context["all_days"][
                0:remaining_days_til_end_of_month
            ]
            context["previous_days"] = context["all_days"][
                remaining_days_til_end_of_month:
            ]

        return context


class HistoryCurrentMonth(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        now = timezone.now()
        return reverse(
            "sleep:history",
            kwargs={
                "year": now.year,
                "month": now.month,
            },
        )


class CreateOtherDay(LoginRequiredMixin, CreateView):
    template_name = "sleep/add_other_day.html"
    model = SleepLogDay
    fields = [
        "lay_down_hour",
        "sleep_start_hour",
        "sleep_end_hour",
        "get_up_hour",
        "quality_of_sleep",
        "condition_on_waking",
        "condition_during_the_day",
        "comment",
    ]

    def form_valid(self, form):
        form.instance.day = datetime.strptime(self.kwargs["day"], "%Y-%m-%d").date()
        form.instance.user = self.request.user
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["day"] = datetime.strptime(self.kwargs["day"], "%Y-%m-%d").date()
        return context

    def get_success_url(self):
        messages.success(
            self.request,
            _(f"The data for day {self.object.day} has been successfully updated."),
        )
        return reverse("sleep:edit_other_day", kwargs={"day": self.object.day})


class EditOtherDay(LoginRequiredMixin, UpdateView):
    template_name = "sleep/other_day.html"
    model = SleepLogDay
    fields = [
        "lay_down_hour",
        "sleep_start_hour",
        "sleep_end_hour",
        "get_up_hour",
        "quality_of_sleep",
        "condition_on_waking",
        "condition_during_the_day",
        "comment",
    ]

    def get_object(self, *args, **kwargs):
        sleep_log_day, created = SleepLogDay.objects.get_or_create(
            user=self.request.user,
            day=datetime.strptime(self.kwargs["day"], "%Y-%m-%d").date(),
        )
        return sleep_log_day

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        current_day = datetime.strptime(self.kwargs["day"], "%Y-%m-%d").date()
        context["sleep_log_day"] = SleepLogDay.objects.filter(
            user=self.request.user, day=current_day
        ).first()
        context["naps"] = Nap.objects.filter(user=self.request.user, day=current_day)
        context["nocturnal_awakenings"] = NocturnalAwakening.objects.filter(
            night=context["sleep_log_day"]
        )
        context["current_day"] = current_day
        context["tomorrow"] = current_day + timedelta(days=1)
        return context

    def get_success_url(self):
        messages.success(
            self.request,
            _(f"The data for day {self.object.day} has been successfully updated."),
        )
        return reverse("sleep:edit_other_day", kwargs={"day": self.object.day})


class AddAwakening(LoginRequiredMixin, CreateView):
    model = NocturnalAwakening
    template_name = "sleep/add_awakening.html"
    fields = [
        "sleep_end_hour",
        "get_up_hour",
        "lay_down_hour",
        "sleep_start_hour",
        "comment",
    ]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["date"] = self.kwargs["day"]
        return context

    def form_valid(self, form):
        day = datetime.strptime(self.kwargs["day"], "%Y-%m-%d").date()
        night = SleepLogDay.objects.get(user=self.request.user, day=day)
        form.instance.night = night
        return super().form_valid(form)

    def get_success_url(self):
        messages.success(
            self.request,
            _(f"Successfully added an awakening for day {self.object.night.day}."),
        )
        if (
            datetime.strptime(self.kwargs["day"], "%Y-%m-%d").date()
            != timezone.now().date
        ):
            return reverse("sleep:edit_other_day", kwargs={"day": self.kwargs["day"]})
        return reverse("sleep:index")


class EditAwakening(LoginRequiredMixin, UpdateView):
    model = NocturnalAwakening
    template_name = "sleep/edit_awakening.html"
    fields = [
        "sleep_end_hour",
        "get_up_hour",
        "lay_down_hour",
        "sleep_start_hour",
        "comment",
    ]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["date"] = self.kwargs["day"]
        return context

    def get_success_url(self):
        messages.success(
            self.request,
            _(f"Edited awakening for day {self.object.night.day}."),
        )
        return reverse(
            "sleep:nocturnal_awakening_list", kwargs={"day": self.kwargs["day"]}
        )


class AddNap(LoginRequiredMixin, CreateView):
    model = Nap
    template_name = "sleep/add_nap.html"
    fields = [
        "lay_down_hour",
        "sleep_start_hour",
        "sleep_end_hour",
        "get_up_hour",
        "comment",
    ]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["date"] = self.kwargs["day"]
        return context

    def form_valid(self, form):
        day = datetime.strptime(self.kwargs["day"], "%Y-%m-%d").date()
        form.instance.day = day
        form.instance.user = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        messages.success(
            self.request,
            _("Successfully added a new nap."),
        )
        return reverse("sleep:index")


class EditNap(LoginRequiredMixin, UpdateView):
    model = Nap
    template_name = "sleep/edit_nap.html"
    fields = [
        "lay_down_hour",
        "sleep_start_hour",
        "sleep_end_hour",
        "get_up_hour",
        "comment",
    ]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["date"] = self.kwargs["day"]
        return context

    def get_success_url(self):
        messages.success(
            self.request,
            _("Nap has been updated successfully."),
        )
        return reverse("sleep:index")
