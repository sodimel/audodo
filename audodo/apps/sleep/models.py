from datetime import timedelta

from django.db import models
from django.utils.translation import gettext_lazy as _

from audodo.apps.user.models import User

LEVELS = (
    ("very bad", _("Very bad")),
    ("bad", _("Bad")),
    ("ok", _("Ok")),
    ("good", _("Good")),
    ("very good", _("Very Good")),
)


class SleepLogDay(models.Model):
    user = models.ForeignKey(
        User,
        related_name="sleep_log_entries",
        on_delete=models.CASCADE,
        verbose_name=_("User"),
    )
    day = models.DateField(verbose_name=_("Day"))
    lay_down_hour = models.TimeField(
        null=True, blank=True, verbose_name=_("Lay down hour")
    )
    sleep_start_hour = models.TimeField(
        null=True, blank=True, verbose_name=_("Sleep start hour")
    )
    sleep_end_hour = models.TimeField(
        null=True, blank=True, verbose_name=_("Sleep end hour")
    )
    get_up_hour = models.TimeField(null=True, blank=True, verbose_name=_("Get up hour"))

    quality_of_sleep = models.CharField(
        max_length=32,
        choices=LEVELS,
        null=True,
        blank=True,
        verbose_name=_("Quality of sleep"),
    )
    condition_on_waking = models.CharField(
        max_length=32,
        choices=LEVELS,
        null=True,
        blank=True,
        verbose_name=_("Condition on waking"),
    )
    condition_during_the_day = models.CharField(
        max_length=32,
        choices=LEVELS,
        null=True,
        blank=True,
        verbose_name=_("Condition during the day"),
    )
    comment = models.CharField(
        max_length=1024, null=True, blank=True, verbose_name=_("Comment")
    )

    @property
    def status_emoji(self):
        if self.is_complete:
            return "✅"
        return "🟧"

    @property
    def tomorrow(self):
        return self.day + timedelta(days=1)

    @property
    def is_complete(self):
        return (
            self.sleep_start_hour
            and self.sleep_end_hour
            and self.lay_down_hour
            and self.get_up_hour
            and self.quality_of_sleep
            and self.condition_on_waking
            and self.condition_during_the_day
        )

    @property
    def sleep_start_hour_no_seconds(self):
        if self.sleep_start_hour:
            return self.sleep_start_hour.isoformat(timespec="minutes")
        return None

    @property
    def sleep_end_hour_no_seconds(self):
        if self.sleep_end_hour:
            return self.sleep_end_hour.isoformat(timespec="minutes")
        return None

    @property
    def lay_down_hour_no_seconds(self):
        if self.lay_down_hour:
            return self.lay_down_hour.isoformat(timespec="minutes")
        return None

    @property
    def get_up_hour_no_seconds(self):
        if self.get_up_hour:
            return self.get_up_hour.isoformat(timespec="minutes")
        return None

    @property
    def get_day_str(self):
        return self.day.strftime("%B %d, %Y")

    @property
    def get_tomorrow_str(self):
        return self.tomorrow.strftime("%B %d, %Y")

    @property
    def simple_str(self):
        if self.is_complete:
            return f"from {self.sleep_start_hour_no_seconds} to {self.sleep_end_hour_no_seconds}"
        return _("Incomplete")

    def __str__(self):
        return (
            _("From")
            + " "
            + self.get_day_str
            + " "
            + _("to")
            + " "
            + self.get_tomorrow_str
        )

    class Meta:
        verbose_name = _("Sleep log day")
        verbose_name_plural = _("Sleep log days")


class NocturnalAwakening(models.Model):
    night = models.ForeignKey(
        SleepLogDay,
        on_delete=models.CASCADE,
        related_name="awakenings",
        verbose_name=_("Night"),
    )
    sleep_end_hour = models.TimeField(verbose_name=_("Sleep end hour"))
    get_up_hour = models.TimeField(
        null=True,
        blank=True,
        help_text=_("Only if you got out of bed."),
        verbose_name=_("Get up hour"),
    )
    lay_down_hour = models.TimeField(
        null=True,
        blank=True,
        help_text=_("Only if you got out of bed."),
        verbose_name=_("Lay down hour"),
    )
    sleep_start_hour = models.TimeField(verbose_name=_("Sleep start hour"))
    comment = models.CharField(
        max_length=1024, null=True, blank=True, verbose_name=_("Comment")
    )

    class Meta:
        verbose_name = _("Nocturnal awakening")
        verbose_name_plural = _("Nocturnal awakenings")


class Nap(models.Model):
    user = models.ForeignKey(
        User,
        related_name="nap_entries",
        on_delete=models.CASCADE,
        verbose_name=_("User"),
    )
    day = models.DateField(verbose_name=_("Day"))
    lay_down_hour = models.TimeField(
        null=True, blank=True, verbose_name=_("Lay down hour")
    )
    sleep_start_hour = models.TimeField(
        null=True, blank=True, verbose_name=_("Sleep start hour")
    )
    sleep_end_hour = models.TimeField(
        null=True, blank=True, verbose_name=_("Sleep end hour")
    )
    get_up_hour = models.TimeField(null=True, blank=True, verbose_name=_("Get up hour"))
    comment = models.CharField(
        max_length=1024, null=True, blank=True, verbose_name=_("Comment")
    )

    class Meta:
        verbose_name = _("Nap")
        verbose_name_plural = _("Naps")
