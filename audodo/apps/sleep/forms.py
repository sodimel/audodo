from django import forms

from .models import SleepLogDay


class SleepLogDayForm(forms.ModelForm):
    class Meta:
        model = SleepLogDay
        exclude = ["user", "day"]
