from django.urls import path
from django.utils.translation import gettext_lazy as _

from .views import (
    AddAwakening,
    AddNap,
    CreateOrUpdateCurrentDay,
    CreateOtherDay,
    EditAwakening,
    EditNap,
    EditOtherDay,
    History,
    HistoryCurrentMonth,
    Index,
)

urlpatterns = [
    path("", Index.as_view(), name="index"),
    path(
        _("current-day/update/"),
        CreateOrUpdateCurrentDay.as_view(),
        name="update_current_day",
    ),
    path(
        _("other-days/edit/<slug:day>/"), EditOtherDay.as_view(), name="edit_other_day"
    ),
    path(
        _("other-days/add/<slug:day>/"),
        CreateOtherDay.as_view(),
        name="create_other_day",
    ),
    path(
        _("history/"),
        HistoryCurrentMonth.as_view(),
        name="history_current_month",
    ),
    path(
        _("history/<int:year>/<int:month>/"),
        History.as_view(),
        name="history",
    ),
    path(
        _("awakenings/add/<slug:day>/"),
        AddAwakening.as_view(),
        name="add_nocturnal_awakening",
    ),
    path(
        _("awakenings/edit/<slug:day>/<int:pk>/"),
        EditAwakening.as_view(),
        name="edit_nocturnal_awakening",
    ),
    path(
        _("nap/add/<slug:day>/"),
        AddNap.as_view(),
        name="add_nap",
    ),
    path(
        _("nap/edit/<slug:day>/<int:pk>/"),
        EditNap.as_view(),
        name="edit_nap",
    ),
]

app_name = "sleep"
