from calendar import monthrange


def get_previous_month(month, year):
    previous_month = month - 1
    previous_year = year
    if previous_month == 0:
        previous_month = 12
        previous_year -= 1
    return previous_month, previous_year


def get_next_month(month, year):
    next_month = month + 1
    next_year = year
    if next_month == 13:
        next_month = 1
        next_year += 1
    return next_month, next_year


def get_all_days_for_month(month, year):
    return list(range(1, monthrange(year, month)[1] + 1))
