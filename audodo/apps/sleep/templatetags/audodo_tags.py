from datetime import timedelta

from django import template
from django.conf import settings

register = template.Library()


@register.simple_tag
def get_version(*args, **kwargs):
    return f"v{settings.VERSION}"


@register.simple_tag
def get_nice_text_for_current_night(day):
    return f"{day} - {day + timedelta(days=1)}"
