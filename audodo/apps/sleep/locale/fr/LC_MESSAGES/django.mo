��    N      �  k   �      �     �  	   �     �     �     �     �     �                5     9     A     Z     n     r  1   �     �  /   �     �     �       
          
   "     -     ;  "   ?  
   b     m  
   r     }     �     �  	   �     �     �     �     �     	     	     $	     3	     D	     V	     e	     s	     �	  
   �	     �	     �	  @   �	  A   
  6   F
     }
     �
     �
  
   �
     �
  	   �
     �
     �
  	   �
  (   �
     �
     �
     �
  $        =     E     Y     b     �     �     �     �     �       A       F     N     a     }     �     �     �     �     �          #     /     D     U  "   Z  X   }     �  0   �            
   #     .     :     K     X     i     p     �     �     �     �  &   �     �     �               .     A     ^  -   a     �     �     �     �     �     �     �          "     )  T   E  P   �  E   �     1     7  #   F     j     �     �     �     �     �  5   �     �     �  %     4   '     \     b     z  "   �     �     �  )   �        $   %     J                  4   6   0   M           1   	       #   ;      8          :                  +       $          @   2   I       >      ?                N              J   *                7   G      &          B           D       ,   K       5          L   /       9         A       
      =           !      .       <   C   (               '       -               H   %   "   )   F      3   E       Add Add a nap Add a nocturnal awakening Add awakening Add data for Add nap Add nocturnal awakening Awakening for night Awakenings for night Bad Comment Condition during the day Condition on waking Day Edit nocturnal awakening Edited awakening for day {self.object.night.day}. From From %(sleep_end_hour)s to %(sleep_start_hour)s Get up hour Good History Incomplete Last 7 days Last month Lay down hour Nap Nap has been updated successfully. Nap on day Naps Next month Night No awakening yet. No data for No nap on No nocturnal awakening Nocturnal awakening Nocturnal awakenings Nocturnal awakenings list Ok Only if you got out of bed. Previous month Quality of sleep Return to history Sleep end hour Sleep log day Sleep log days Sleep start hour Statistics Submit Successfully added a new nap. Successfully added an awakening for day {self.object.night.day}. The data for day {self.object.day} has been successfully updated. There was an error or more on your form: {form.errors} Total Update Update data for Update nap User Very Good Very bad You  You slept Your current day data have been updated. add and awakenings/add/<slug:day>/ awakenings/edit/<slug:day>/<int:pk>/ between current-day/update/ history/ history/<int:year>/<int:month>/ hours per night, with nap/add/<slug:day>/ nap/edit/<slug:day>/<int:pk>/ other-days/add/<slug:day>/ other-days/edit/<slug:day>/ to Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Ajouter Ajouter une sieste Ajouter un réveil nocturne Ajouter un réveil nocturne Ajouter des données pour Ajouter une sieste Ajouter un réveil nocturne Réveils durant la nuit Réveils nocturnes pour la nuit Mauvaise Commentaire Forme de la journée État au réveil Jour Mettre à jour le réveil nocturne Le réveil nocturne de la nuit {self.object.night.day} a été mis à jour avec succès. Depuis Entre %(sleep_end_hour)s et %(sleep_start_hour)s Heure du lever Bonne Historique Incomplète 7 derniers jours Mois dernier Heure du coucher Sieste La sieste a été mise à jour. Siestes du jour Siestes Mois suivant Nuit Aucun réveil nocturne pour l'instant. Aucune donnée pour Aucune sieste le Aucun réveil nocturne Réveil nocturne Réveils nocturnes Liste des réveils nocturnes Ok Seulement si vous vous êtes levé(e) du lit. Mois précédent Qualité du sommeil Retour à l'historique Heure de réveil Entrée de nuit Entrées de nuits Heure d'endormissement Statistiques Envyer La sieste a été ajoutée. Le réveil nocturne de la nuit {self.object.night.day} a été ajouté avec succès. Les données pour le jour {self.object.day} ont été mise à jour avec succès. Il y a une ou plusieurs erreurs dans votre formulaire : {form.errors} Total Mettre à jour Mettre à jour les données pour le Mettre à jour la sieste Utilisateur Très bonne Très mauvaise Vous  Vous avez dormi Les données du jour courant ont été mises à jour. ajouter et reveils-nocturnes/ajouter/<slug:day>/ reveils-nocturnes/mettre-a-jour/<slug:day>/<int:pk>/ entre aujourdhui/mise-a-jour/ historique/ historique/<int:year>/<int:month>/ heures par nuit, avec sieste/ajouter/<slug:day>/ sieste/mettre-a-jour/<slug:day>/<int:pk>/ autres-jours/ajouter/<slug:day>/ autres-jours/mise-a-jour/<slug:day>/ jusqu'au 