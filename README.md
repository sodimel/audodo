<div align="center">
    <img src="audodo/static/images/logo.png" alt="Audodo logo" />
</div>

Simple sleep log website. Also a PWA that you can instal on your phone (with offline support).

> Version 0.0.3

----

## Screenshots

| ![Homepage](https://gitlab.com/sodimel/audodo/uploads/e7eb7e6bdea0f89452be220ba909b4b3/homepage.jpg) | ![Dashboard](https://gitlab.com/sodimel/audodo/uploads/a5124e16bd5145b4fe8dd92a17e92c66/dashboard.jpg) | ![History](https://gitlab.com/sodimel/audodo/uploads/500ec0cd92f9ee1bfeaee927366a5060/history.jpg) |
| ---      | ---      | ---- |
| Homepage | Dashboard | History |


## [Deploy an instance of audodo](DEPLOY.md)

## Config

Config options that you can put in your `.env` file:

- `AUDODO_LANGUAGE_CODE` (default: "en"): Default language of your instance.
- `AUDODO_SECRET_KEY` (default: None): Only needed on production, see [DEPLOY.md](DEPLOY.md) for more info.
- `AUDODO_ALLOWED_HOSTS` (default: None): Only needed on production, see [DEPLOY.md](DEPLOY.md) for more info.

## Develop

1) Clone
    ```bash
    git clone git@gitlab.com:sodimel/audodo.git
    cd audodo
    ```

2) Create venv, .env, source the venv & .env
    ```bash
    python3 -m venv .venv
    echo "export DJANGO_SETTINGS_MODULE=audodo.settings.dev" > .env
    . .venv/bin/activate
    source .env
    ```

3) Install requirements
    ```bash
    python3 -m pip install -r requirements.txt -r requirements_dev.txt
    ```

4) Install pre-commit
    ```bash
    pre-commit install
    ```

5) Create the db (migrate)
    ```bash
    python3 manage.py migrate
    ```

6) Create a super user
    ```bash
    python3 manage.py createsuperuser
    ```

7) Launch the website
    ```bash
    python3 manage.py runserver 0.0.0.0:8000
    ```

8) That's all folks!
